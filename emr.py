# EMR Data

import boto3


my_bucket = 's3://aa070d3e123'
LogUri = my_bucket + '/logs'
Ec2KeyName = 'emr'
EmrManagedMasterSecurityGroup = 'sg-07c8b26111f60c2b5'
EmrManagedSlaveSecurityGroup = 'sg-0c2ecb8e2d98f9062'
ActionOnFailure = 'CANCEL_AND_WAIT'
emr_EC2_DefaultRole = 'EMR_EC2_DefaultRole'
emr_DefaultRole = 'EMR_DefaultRole'


def emr_submit_job():
    emrClient = boto3.client('emr', region_name='us-west-2')
    response = emrClient.run_job_flow(
        Name='Boto3 Cluster',
        LogUri=LogUri,
        ReleaseLabel='emr-5.14.0',
        Instances={
            'InstanceGroups': [
                {
                    'Name': 'master',
                    'Market': 'ON_DEMAND',
                    'InstanceRole': 'MASTER',
                    'InstanceType': 'm4.xlarge',
                    'InstanceCount': 1
                },
                {
                    'Name': 'core',
                    'Market': 'ON_DEMAND',
                    'InstanceRole': 'CORE',
                    'InstanceType': 'm4.xlarge',
                    'InstanceCount': 2
                }
            ],
            'Ec2KeyName': Ec2KeyName,
            'KeepJobFlowAliveWhenNoSteps': True,
            'EmrManagedMasterSecurityGroup': EmrManagedMasterSecurityGroup,
            'EmrManagedSlaveSecurityGroup': EmrManagedSlaveSecurityGroup
        },
        Steps=[
            {
                'Name': 'Setup hadoop debugging',
                'ActionOnFailure': ActionOnFailure,
                'HadoopJarStep': {
                    'Jar': 'command-runner.jar',
                    'Args': [
                        'state-pusher-script'
                    ]
                }
            }
        ],
        VisibleToAllUsers=True,
        JobFlowRole=emr_EC2_DefaultRole,
        ServiceRole=emr_DefaultRole,
        EbsRootVolumeSize=10
    )

    # I will need cluster id
    myclusterID = response['JobFlowId']

    # Create waiter
    wait_for_running_cluster = emrClient.get_waiter('cluster_running')

    # what is the cluster status now
    getclusterstatus = emrClient.describe_cluster(
        ClusterId=myclusterID
    )

    # let's wait until cluster is in running state
    wait_for_running_cluster.wait(
        ClusterId=myclusterID,
        WaiterConfig={
            'Delay': 30,
            'MaxAttempts': 2
        }
    )

    # trust but verify, what is the cluster status now?
    print(print(getclusterstatus['Cluster']['Status']['State']))

    # get master public name
    print(getclusterstatus['Cluster']['MasterPublicDnsName'])


if __name__ == '__main__':
    # Launch small cluster
    emr_submit_job()
